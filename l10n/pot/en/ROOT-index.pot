# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Translator
# This file is distributed under the same license as the Antora package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Antora 1.0\n"
"POT-Creation-Date: 2022-10-16 09:57+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Positional ($1) AttributeList argument for macro 'image'
#: docs/en/modules/ROOT/pages/index.adoc:1
#: docs/en/modules/ROOT/pages/index.adoc:4
#: docs/en/modules/ROOT/pages/index.adoc:17
#, no-wrap
msgid "Universal Declaration of Human Rights"
msgstr ""

#. type: Attribute :keywords:
#: docs/en/modules/ROOT/pages/index.adoc:6
#, no-wrap
msgid "\"Human rights\", \"United Nations\", udhr"
msgstr ""

#. type: Attribute :lang:
#: docs/en/modules/ROOT/pages/index.adoc:8
#, no-wrap
msgid "en"
msgstr ""

#. type: Attribute :page-lang:
#: docs/en/modules/ROOT/pages/index.adoc:9
#, no-wrap
msgid "{lang}"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:13
msgid "{copyright}"
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:15
#, no-wrap
msgid "Preamble"
msgstr ""

#. type: Target for macro image
#: docs/en/modules/ROOT/pages/index.adoc:17
#, no-wrap
msgid "en@antora-i18n:ROOT:udhr.svg"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:20
msgid ""
"Whereas recognition of the inherent dignity and of the equal and inalienable "
"rights of all members of the human family is the foundation of freedom, "
"justice and peace in the world,"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:22
msgid ""
"Whereas disregard and contempt for human rights have resulted in barbarous "
"acts which have outraged the conscience of mankind, and the advent of a "
"world in which human beings shall enjoy freedom of speech and belief and "
"freedom from fear and want has been proclaimed as the highest aspiration of "
"the common people,"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:24
msgid ""
"Whereas it is essential, if man is not to be compelled to have recourse, as "
"a last resort, to rebellion against tyranny and oppression, that human "
"rights should be protected by the rule of law,"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:26
msgid ""
"Whereas it is essential to promote the development of friendly relations "
"between nations,"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:28
msgid ""
"Whereas the peoples of the United Nations have in the Charter reaffirmed "
"their faith in fundamental human rights, in the dignity and worth of the "
"human person and in the equal rights of men and women and have determined to "
"promote social progress and better standards of life in larger freedom,"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:30
msgid ""
"Whereas Member States have pledged themselves to achieve, in co‐operation "
"with the United Nations, the promotion of universal respect for and "
"observance of human rights and fundamental freedoms,"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:32
msgid ""
"Whereas a common understanding of these rights and freedoms is of the "
"greatest importance for the full realization of this pledge, Now, therefore, "
"The General Assembly Proclaims this Universal Declaration of Human Rights as "
"a common standard of achievement for all peoples and all nations, to the end "
"that every individual and every organ of society, keeping this Declaration "
"constantly in mind, shall strive by teaching and education to promote "
"respect for these rights and freedoms and by progressive measures, national "
"and international, to secure their universal and effective recognition and "
"observance, both among the peoples of Member States themselves and among the "
"peoples of territories under their jurisdiction."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:33
#, no-wrap
msgid "Article 1 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:37
msgid ""
"All human beings are born free and equal in dignity and rights.  They are "
"endowed with reason and conscience and should act towards one another in a "
"spirit of brotherhood."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:38
#, no-wrap
msgid "Article 2 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:41
msgid ""
"Everyone is entitled to all the rights and freedoms set forth in this "
"Declaration, without distinction of any kind, such as race, colour, sex, "
"language, religion, political or other opinion, national or social origin, "
"property, birth or other status."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:43
msgid ""
"Furthermore, no distinction shall be made on the basis of the political, "
"jurisdictional or international status of the country or territory to which "
"a person belongs, whether it be independent, trust, non‐self‐governing or "
"under any other limitation of sovereignty."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:44
#, no-wrap
msgid "Article 3 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:47
msgid "Everyone has the right to life, liberty and the security of person."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:48
#, no-wrap
msgid "Article 4 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:51
msgid ""
"No one shall be held in slavery or servitude; slavery and the slave trade "
"shall be prohibited in all their forms."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:52
#, no-wrap
msgid "Article 5 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:55
msgid ""
"No one shall be subjected to torture or to cruel, inhuman or degrading "
"treatment or punishment."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:56
#, no-wrap
msgid "Article 6"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:59
msgid ""
"Everyone has the right to recognition everywhere as a person before the law."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:60
#, no-wrap
msgid "Article 7 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:64
msgid ""
"All are equal before the law and are entitled without any discrimination to "
"equal protection of the law.  All are entitled to equal protection against "
"any discrimination in violation of this Declaration and against any "
"incitement to such discrimination."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:65
#, no-wrap
msgid "Article 8 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:68
msgid ""
"Everyone has the right to an effective remedy by the competent national "
"tribunals for acts violating the fundamental rights granted him by the "
"constitution or by law."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:69
#, no-wrap
msgid "Article 9"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:72
msgid "No one shall be subjected to arbitrary arrest, detention or exile."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:73
#, no-wrap
msgid "Article 10 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:76
msgid ""
"Everyone is entitled in full equality to a fair and public hearing by an "
"independent and impartial tribunal, in the determination of his rights and "
"obligations and of any criminal charge against him."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:77
#, no-wrap
msgid "Article 11 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:80
msgid ""
"Everyone charged with a penal offence has the right to be presumed innocent "
"until proved guilty according to law in a public trial at which he has had "
"all the guarantees necessary for his defence."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:81
msgid ""
"No one shall be held guilty of any penal offence on account of any act or "
"omission which did not constitute a penal offence, under national or "
"international law, at the time when it was committed."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:82
msgid ""
"Nor shall a heavier penalty be imposed than the one that was applicable at "
"the time the penal offence was committed."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:83
#, no-wrap
msgid "Article 12 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:87
msgid ""
"No one shall be subjected to arbitrary interference with his privacy, "
"family, home or correspondence, nor to attacks upon his honour and "
"reputation.  Everyone has the right to the protection of the law against "
"such interference or attacks."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:88
#, no-wrap
msgid "Article 13 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:91
msgid ""
"Everyone has the right to freedom of movement and residence within the "
"borders of each State."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:92
msgid ""
"Everyone has the right to leave any country, including his own, and to "
"return to his country."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:93
#, no-wrap
msgid "Article 14 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:96
msgid ""
"Everyone has the right to seek and to enjoy in other countries asylum from "
"persecution."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:97
msgid ""
"This right may not be invoked in the case of prosecutions genuinely arising "
"from non‐political crimes or from acts contrary to the purposes and "
"principles of the United Nations."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:98
#, no-wrap
msgid "Article 15"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:101
msgid "Everyone has the right to a nationality."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:102
msgid ""
"No one shall be arbitrarily deprived of his nationality nor denied the right "
"to change his nationality."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:103
#, no-wrap
msgid "Article 16"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:107
msgid ""
"Men and women of full age, without any limitation due to race, nationality "
"or religion, have the right to marry and to found a family.  They are "
"entitled to equal rights as to marriage, during marriage and at its "
"dissolution."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:108
msgid ""
"Marriage shall be entered into only with the free and full consent of the "
"intending spouses."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:109
msgid ""
"The family is the natural and fundamental group unit of society and is "
"entitled to protection by society and the State."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:110
#, no-wrap
msgid "Article 17"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:113
msgid ""
"Everyone has the right to own property alone as well as in association with "
"others."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:114
msgid "No one shall be arbitrarily deprived of his property."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:115
#, no-wrap
msgid "Article 18 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:118
msgid ""
"Everyone has the right to freedom of thought, conscience and religion; this "
"right includes freedom to change his religion or belief, and freedom, either "
"alone or in community with others and in public or private, to manifest his "
"religion or belief in teaching, practice, worship and observance."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:119
#, no-wrap
msgid "Article 19"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:122
msgid ""
"Everyone has the right to freedom of opinion and expression; this right "
"includes freedom to hold opinions without interference and to seek, receive "
"and impart information and ideas through any media and regardless of "
"frontiers."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:123
#, no-wrap
msgid "Article 20"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:126
msgid "Everyone has the right to freedom of peaceful assembly and association."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:127
msgid "No one may be compelled to belong to an association."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:128
#, no-wrap
msgid "Article 21"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:131
msgid ""
"Everyone has the right to take part in the government of his country, "
"directly or through freely chosen representatives."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:132
msgid ""
"Everyone has the right of equal access to public service in his country."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:133
msgid ""
"The will of the people shall be the basis of the authority of government; "
"this will shall be expressed in periodic and genuine elections which shall "
"be by universal and equal suffrage and shall be held by secret vote or by "
"equivalent free voting procedures."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:134
#, no-wrap
msgid "Article 22 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:137
msgid ""
"Everyone, as a member of society, has the right to social security and is "
"entitled to realization, through national effort and international co‐"
"operation and in accordance with the organization and resources of each "
"State, of the economic, social and cultural rights indispensable for his "
"dignity and the free development of his personality."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:138
#, no-wrap
msgid "Article 23 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:141
msgid ""
"Everyone has the right to work, to free choice of employment, to just and "
"favourable conditions of work and to protection against unemployment."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:142
msgid ""
"Everyone, without any discrimination, has the right to equal pay for equal "
"work."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:143
msgid ""
"Everyone who works has the right to just and favourable remuneration "
"ensuring for himself and his family an existence worthy of human dignity, "
"and supplemented, if necessary, by other means of social protection."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:144
msgid ""
"Everyone has the right to form and to join trade unions for the protection "
"of his interests."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:145
#, no-wrap
msgid "Article 24 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:148
msgid ""
"Everyone has the right to rest and leisure, including reasonable limitation "
"of working hours and periodic holidays with pay."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:149
#, no-wrap
msgid "Article 25"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:151
msgid ""
"Everyone has the right to a standard of living adequate for the health and "
"well‐being of himself and of his family, including food, clothing, housing "
"and medical care and necessary social services, and the right to security in "
"the event of unemployment, sickness, disability, widowhood, old age or other "
"lack of livelihood in circumstances beyond his control."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:153
msgid ""
"Motherhood and childhood are entitled to special care and assistance.  All "
"children, whether born in or out of wedlock, shall enjoy the same social "
"protection."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:154
#, no-wrap
msgid "Article 26 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:160
msgid ""
"Everyone has the right to education.  Education shall be free, at least in "
"the elementary and fundamental stages.  Elementary education shall be "
"compulsory.  Technical and professional education shall be made generally "
"available and higher education shall be equally accessible to all on the "
"basis of merit."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:162
msgid ""
"Education shall be directed to the full development of the human personality "
"and to the strengthening of respect for human rights and fundamental "
"freedoms.  It shall promote understanding, tolerance and friendship among "
"all nations, racial or religious groups, and shall further the activities of "
"the United Nations for the maintenance of peace."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:163
msgid ""
"Parents have a prior right to choose the kind of education that shall be "
"given to their children."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:164
#, no-wrap
msgid "Article 27 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:167
msgid ""
"Everyone has the right freely to participate in the cultural life of the "
"community, to enjoy the arts and to share in scientific advancement and its "
"benefits."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:168
msgid ""
"Everyone has the right to the protection of the moral and material interests "
"resulting from any scientific, literary or artistic production of which he "
"is the author."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:169
#, no-wrap
msgid "Article 28 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:172
msgid ""
"Everyone is entitled to a social and international order in which the rights "
"and freedoms set forth in this Declaration can be fully realized."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:173
#, no-wrap
msgid "Article 29"
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:175
msgid ""
"Everyone has duties to the community in which alone the free and full "
"development of his personality is possible."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:176
msgid ""
"In the exercise of his rights and freedoms, everyone shall be subject only "
"to such limitations as are determined by law solely for the purpose of "
"securing due recognition and respect for the rights and freedoms of others "
"and of meeting the just requirements of morality, public order and the "
"general welfare in a democratic society."
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:177
msgid ""
"These rights and freedoms may in no case be exercised contrary to the "
"purposes and principles of the United Nations."
msgstr ""

#. type: Title ==
#: docs/en/modules/ROOT/pages/index.adoc:178
#, no-wrap
msgid "Article 30 "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/index.adoc:180
msgid ""
"Nothing in this Declaration may be interpreted as implying for any State, "
"group or person any right to engage in any activity or to perform any act "
"aimed at the destruction of any of the rights and freedoms set forth herein."
msgstr ""
