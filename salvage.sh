#!/usr/bin/bash
sourcefile=$1
localized=$2
# source/target translation language(s)
source="en"
target="fr"

#asciidoc options
OPTIONS="tablecells"
#OPTIONS="-o tablecells"
#--option debug split_attributelist -l content/en/post/anki.adoc -p l10n/po/en/post/anki.po
OPTIONS="-k 0 tablecells entry='lang page-lang description page-tags tags categories note-caption tip-caption warning-caption description'"
ME="Translator"
PACKAGE="Blog"
VERSION="1.0"
POT_DIR="./l10n/pot"
PO_DIR="./l10n/po"

[[ ! -f $sourcefile ]] && { echo invalid source file ; exit 1 ; } ; 
[[ ! -f $localized ]] && { echo invalid localized file ; exit 1 ; } ; 

# repository root location
content="./docs"
[[ ! -d "$content" ]] &&  { echo "Error, please check that Scontent matches the repository root" ; exit 1 ; }

basename=$(basename -s .adoc "$sourcefile")
fullpath=$(dirname "$sourcefile")
path=$(dirname ${sourcefile#${content#./}/})
#minus the content base and file name: for example /fr/post
subdir=${path#$source}
#minus the language for example /post

pot_file="$POT_DIR/$source/$basename.adoc.pot"
po_file="$PO_DIR/$target/$basename.adoc.po"
#localized="${file/\/$source/\/$target}"

[[ ! -f $pot_file ]]  && { echo "We need a master po file.  Start by entering the file in po4a.cfg and runnung po_update. Exiting..." ; exit 1 ; }
[[ -f $po_file ]]  && { echo "PO file exists already. I don't want to overwrite it. Exiting..." ; exit 1 ; }

sed -i '/X-Source-Language:.*/d' $pot_file &&  sed -i "/Content-Type:/a \"X-Source-Language: $source $(printf '%q' '\n')\"" $pot_file 
echo Added source language to $pot_file

po4a-gettextize \
  --format asciidoc --option $OPTIONS \
  --master "$sourcefile" \
  --localized "$localized" \
  --master-charset "UTF-8" \
  --localized-charset "UTF-8" \
  --copyright-holder "$ME" \
  --package-name "$PACKAGE" \
  --package-version "$VERSION" \
  --po "$po_file" \
  --verbose 

echo Updated $po_file
sed -i '/X-Source-Language:.*/d' $po_file &&  sed -i "/Content-Type:/a \"X-Source-Language: $source $(printf '%q' '\n')\"" $po_file 
sed -i "s/^\"Language:.*$/\"Language: $target $(printf '%q' '\n')\"/g" $po_file 
echo Added source and target languages to $po_file
git add $po_file $pot_file $localized -f
